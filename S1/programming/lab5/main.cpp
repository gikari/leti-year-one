#include <iostream>
#include <fstream>

using namespace std;

int is_delim(char ch, char* ds);

int main()
{
    char delim_str[32];
    char * char_arr;
    int l;
    int i;
    
    int wl, wc, lc, cwl;
    
    cout << "Введите строку разделителей: ";
    cin.getline(delim_str, 32);
    cout << "Введите длину массива символов: ";
    do
        cin >> l;
    while (l <= 0);
    
    char_arr = new char[l];
    
    cout << "Введите массив символов: " << endl;
    for (i = 0; i < l; i++)
    {
        cout << "chat_arr[" << i << "]: ";
        cin >> char_arr[i];
    }
    
    cout << delim_str << endl;
    for (i = 0; i < l; i++)
        cout << char_arr[i];
    cout << endl << l << endl;
    
    wl = lc = 0;
    
    for(i = l-1; i >= 0; i--)
        if (!is_delim(char_arr[i], delim_str))
            wl++;
        else if (is_delim(char_arr[i], delim_str) && wl != 0)
        {
            lc = i;
            break;
        }
    cout << "Длина последнего слова: " << wl << endl;
    
    
    if (wl != 0)
    {
        wc = 1;
        cwl = 0;
        for (i = lc; i >= 0; i--)
        {
            if (is_delim(char_arr[i], delim_str))
            {
                if (cwl == wl)
                    wc++;
                cwl = 0;
            }
            else
            {
                cwl++;
                if (i == 0 && cwl == wl && i != lc)
                    wc++;
            }
        }
    }
    else
    {
        wc = 0;
    }
    
    
    
    cout << "Количество слов с длиной последнего: " << wc << endl;
    
    delete[] char_arr;
    cout << endl << "Выполнение завершено! (Alt + F4 для выхода)";
    return 0;
}

int is_delim(char ch, char* ds)
{
    int i;
    for (i = 0; ds[i] != '\0'; i++)
        if (ch == ds[i]) return 1;
    return 0;
}

