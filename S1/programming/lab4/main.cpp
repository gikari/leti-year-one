#include <iostream>

using namespace std;

int main()
{
    int* A;
    int n, x;
    int i, i2, t;

    cout << "Input n: ";
    do
        cin >> n;
    while (n <= 0);

    A = new int[n];

    for (i = 0; i < n; i++)
    {
        cout << "Input A[" << i << "]: ";
        cin >> A[i];
    }

    cout << "Input x: ";
    cin >> x;


    cout << "( ";
    for (i = 0; i < n; i++)
    {
        cout << A[i] << ", ";
    }
    cout << "); " << endl;

    i = i2 = 0;

    while (i2 < n)
    {
        if (A[i2] >= x)
        {
            while (i2 - i > 0)
            {
                t = A[i2];
                A[i2] = A[i2 - 1];
                A[i2 - 1] = t;
                i2--;
            }
            i2 = ++i;
        }
        else
            i2++;
    }

    cout << "( ";
    for (i = 0; i < n; i++)
    {
        cout << A[i] << ", ";
    }
    cout << "); " << endl;

    delete [] A;

    return 0;
}
