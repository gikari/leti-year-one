#include <iostream>
#include <iomanip>

using namespace std;

void autof ( int* row, int row_len, int i );
void print_row ( int* row, int row_len );
int row_sum ( int *row, int row_len );

int main()
{
    int a, m, n;
    int i, j, k;
    int **A = nullptr, **B = nullptr, *old_Bstr = nullptr;
    int s;

    do
    {
        cout << "Введите число: ";
        cin >> a;
    }
    while ( a <= 0 );

    do
    {
        cout << "Введите количество строк массива: ";
        cin >> m;
    }
    while ( m <= 0 );

    A = new int*[m];

    do
    {
        cout << "Введите количество столбцов массива: ";
        cin >> n;
    }
    while ( n <= 0 );

    for ( i = 0; i < m; i++ )
        A[i] = new int[n];


    for ( i = 0; i < m; i++ )
    {
        s = 0;
        for ( j = 0; j < n; j++ )
        {
            cout << "Введите А[" << i << "][" << j << "]: ";
            cin >> A[i][j];
            if ( A[i][j] < 0 )
            {
                s++;
                if ( s == a )
                {
                    autof ( A[i], n, j+1 );
                    break;
                }
            }
        }
        cout << endl;
    }

    for ( i = 0; i < m; i++ )
    {
        print_row ( A[i], n );
    }
    cout << endl;

    B = new int*[n];
    s = 0;
    for ( i = 0; i < m; i++ )
    {
        if ( row_sum ( A[i], n ) != 0 )
        {
            for ( j = 0; j < n; j++ )
            {
                old_Bstr = B[j];
                B[j] = new int[s+1];
                for ( k = 0; k < s; k++ )
                    B[j][k] = old_Bstr[k];

                delete [] old_Bstr;
                old_Bstr = nullptr;
            }
            for ( j = 0; j < n; j++ )
                B[j][s] = A[i][j];
            s++;
        }
    }


    if ( s != 0 )
    {
        for ( i = 0; i < n; i++ )
            print_row ( B[i], s );
    }
    else
    {
        cout << "Нельзя сформировать массив." << endl;
    }

    for ( i = 0; i < m; i++ )
        delete [] A[i];
    delete [] A;

    for ( i = 0; i < s; i++ )
        delete [] B[i];
    delete [] B;

    cout << "Выполнение завершено. (Alt + F4 для выхода)" << endl;
    return 0;
}


void autof ( int* row, int row_len, int i )
{
    int j;
    for ( j = i; j < row_len; j++ )
        row[j] = row[0];
}


void print_row ( int* row, int row_len )
{
    int j;
    for ( j = 0; j < row_len; j++ )
    {
        cout << setw ( 3 );
        cout << row[j] << " ";
    }
    cout << endl;
}


int row_sum ( int* row, int row_len )
{
    int i, s;
    s = 0;
    for ( i = 0; i < row_len; i++ )
        s += row[i];
    return s;
}
