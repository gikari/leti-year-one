#pragma once
/**
 * @brief Вычисляет длинну строки
 *
 * @param str Строка, чья длина вычисляется
 * @return int
 */
int strlen ( char *str );

/**
 * @brief Копирует одну строку в другую
 *
 * @param str_src Копируемая строка-источник
 * @param str_dest Строка-назначение, в которую копируют строку источник
 * @return void
 */
void strcpy ( char *str_src, char *str_dest );

/**
 * @brief Проверяет имеет ли строка все символы разделители
 *
 * @param str Строка, которая проверяется на наличие разделителей
 * @param str_delims Массив символов разделителей
 * @param delims_l Длина массива разделителей
 * @return bool
 */
bool has_all_delims ( char* str, char* str_delims, int delims_len );

/**
 * @brief Проверяет является ли символ разделителем
 *
 * @param ch Проверяемый символ
 * @param str_delims Строка Массив символов разделителей
 * @param delims_len Длина массива разделителей
 * @return bool
 */
bool is_delim ( char ch, char* str_delims, int delims_len );

/**
 * @brief Меняет два слова в строке местами
 *
 * @param str Строка со словами
 * @param word1_begin Индекс первого символа первого слова
 * @param word1_len Длина первого слова
 * @param word2_begin Индекс первого символа второго слова
 * @param word2_len Длина второго слова
 * @return void
 */
void swap_words ( char* str, int word1_begin, int word1_len,
                  int word2_begin, int word2_len );

/**
 * @brief Вычисляет длину и индекс первого символа длиннейшего слова в строке
 *
 * @param str Строка в которой ищется самое длинное слово
 * @param delims Массив символов разделителей
 * @param delims_len Длина массива символов разделителей
 * @param start Символ с которого искать длиннейшее слово
 * @param index_ptr Адрес переменной, в которую записать индекс \
 *      первого сивола длиннейшего слова
 * @param len_ptr Адрес переменной, в которую записать длину \
 *      первого сивола длиннейшего слова
 * @return void
 */
void get_max_word ( char* str, char* delims, int delims_len,
                    int start, int* index_ptr, int* len_ptr );

/**
 * @brief Вычисляет длину и индекс первого слова в строке
 *
 * @param str Строка, в которой ищется слово
 * @param delims Массив разделителей
 * @param delims_len Длина массива разделителей
 * @param start Индекс с которого начинается поиск
 * @param index_ptr Адрес переменной в которой записать индекс
 * @param len_ptr Адрес переменной в которую записать длину
 * @return void
 */
void get_first_word ( char* str, char* delims, int delims_len,
                      int start, int* index_ptr, int* len_ptr );

/**
 * @brief Удаляет строку из массива со сдвигом
 *
 * @param str_arr Массив строк из которого происходит удаление
 * @param str_arr_len Длина массива строк
 * @param index Номер строки, которая удаляется
 * @return void
 */
void del_str_from_arr ( char** str_arr, int str_arr_len, int index );
