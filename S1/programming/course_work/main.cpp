#include <iostream>
#include <malloc.h>
#include "functions.hpp"

using namespace std;

void input ( int& delims_len, char*& delims, char**& strings, int& n );
char** reformat ( int delims_len, char* delims, char**& strings, int n, int& result_n );
void output ( char** strings, int n );

int main()
{
    char **strings = nullptr;
    char **new_strings = nullptr;
    char *delims = nullptr;
    int delims_len, n, new_n;
    int i;

    bool run = true;
    char action = '0';
    while ( run )
    {
        cout
                << "1 - Ввести данные для обработки" << endl
                << "2 - Обработать введенные ранее данные" << endl
                << "3 - Вывести результат обработки" << endl
                << "0 - Выход" << endl
                << "Выберите действие: ";
        cin >> action;
        switch ( action )
        {
        case '0':
            run = false;
            break;
        case '1':
            if ( delims != nullptr || strings != nullptr )
            {
                cout << "Ввести исходные данные? Предыдущие данные "
                     << "будут ПОТЕРЯНЫ! (y\\n)" << endl;
                cin >> action;
            }
            else
            {
                action = 'y';
            }
            switch ( action )
            {
            case 'y':
                input ( delims_len, delims, strings, n );
                cout << "Записано." << endl;
                break;
            case 'n':
            default:
                cout << "Отмена." << endl;
                break;
            }
            break;
        case '2':
            if ( strings != nullptr && delims != nullptr )
            {
                new_strings = reformat ( delims_len, delims, strings, n, new_n );
                cout << "Обработка завершена." << endl;
            }
            else
            {
                cout << "Недостаточно исходных данных. ";
                cout << "Не хватает ";
                if ( strings == nullptr && delims == nullptr )
                {
                    cout << "строк для обработки и массива разделителей."
                         << endl;
                }
                else if ( delims == nullptr )
                {
                    cout << "массива разделителей." << endl;
                }
                else
                {
                    cout << "строк для обработки." << endl;
                }
            }
            break;
        case '3':
            if ( new_strings != nullptr )
            {
                output ( new_strings, new_n );
                cout << endl;
            }
            else
            {
                cout << "Недостаточно данных для вывода." << endl;
            }
            break;
        default:
            break;
        }

    }

    cout << endl << "Выполнение завершено! (Alt + F4 для выхода)";
    free ( delims );
    for ( i = 0; i < n; i++ )
        free ( strings[i] );
    free ( strings );
    return 0;
}

void input ( int& delims_len, char*& delims, char**& strings, int& n )
{
    int i, k, action, all;
    cout << "Что ввести\\изменить? " << endl
         << "1 - Массив разделителей " << endl
         << "2 - Строки " << endl
         << "0 - Всё " << endl; 
    cin >> action;
    cin.ignore ( 10000, '\n' );
    all = 0;
    switch (action)
    {
        case 0:
            all = 1;
        case 1:
            cout << "Введите длину массива разделителей: ";
            cin >> delims_len;
            cin.ignore ( 10000, '\n' );
            if (delims != nullptr) {free(delims); delims = nullptr;}
            delims = ( char* ) malloc ( delims_len*sizeof ( char ) );

            for ( i = 0; i < delims_len; i++ )
            {
                cout << "Введите разделитель [" << i << "]: ";
                cin >> delims[i];
            }
            cin.ignore ( 10000, '\n' );
            if (!all) break;
        case 2:
            n = 0;
            do
            {
                n++;
                strings = ( char** ) realloc ( strings, n*sizeof ( char* ) );
                cout << "Введите новую строку: ";
                k = 0;
                do
                {
                    k++;
                    strings[n - 1] = ( char* )realloc(strings[n - 1], k*sizeof(char));
                    cin.get ( strings[n - 1][k-1] );
                }
                while ( strings[n - 1][k-1] != '\n' );
                strings[n - 1][k-1] = '\0';
            }
            while ( !has_all_delims ( strings[n - 1], delims, delims_len ) );

            cout << endl;
            for ( i = 0; i < n; i++ )
                cout << strings[i] << endl;
            cout << endl;
            break;
        
        default:
            cout << "Данные не введены" << endl;
    }

    

}

char** reformat ( int delims_len, char* delims, char**& strings,
                  int n, int& result_n )
{
    int max_word_index, max_word_len;
    int first_word_index, first_word_len;
    int swaps, i, j;

    char ** _strings = ( char** ) malloc ( n*sizeof ( char* ) );
    for ( i = 0; i < n; i++ )
    {
        _strings[i] = ( char* ) malloc ( 
            ( strlen ( strings[i] ) +1 ) *sizeof ( char ) );
        strcpy ( strings[i], _strings[i] );
    }

    for ( i = 0; i < n; i++ )
    {
        j = swaps = 0;
        while ( _strings[i][j] != '\0' )
        {
            get_max_word ( _strings[i], delims, delims_len, j, 
                           &max_word_index, &max_word_len );
            get_first_word ( _strings[i], delims, delims_len, j, 
                             &first_word_index, &first_word_len );

            if ( max_word_len == 0 || first_word_len == 0 )
            {
                break;
            }

            if ( max_word_len > first_word_len && 
                max_word_index > first_word_index )
            {
                swap_words ( _strings[i], first_word_index, first_word_len, 
                             max_word_index, max_word_len );
                j = first_word_index + max_word_len;
                swaps++;
            }
            else
            {
                j = first_word_index + first_word_len;
            }
        }
        if ( swaps == 0 )
        {
            del_str_from_arr ( _strings, n, i );
            i--;
            n--;
        }
    }

    result_n = n;
    return _strings;
}

void output ( char** strings, int n )
{
    int i;
    cout << endl;
    for ( i = 0; i < n; i++ )
        cout << strings[i] << endl;
}