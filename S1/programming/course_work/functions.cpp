#include "functions.hpp"
#include <malloc.h>

int strlen ( char *str )
{
    int i;

    i = 0;
    while ( str[i] != '\0' )
        i++;

    return ( i + 1 );
}

void strcpy ( char *str_src, char *str_dest )
{
    int i;

    for ( i = 0; str_src[i] != '\0'; i++ )
        str_dest[i] = str_src[i];

    str_dest[i] = '\0';
}

bool has_all_delims ( char* str, char* str_delims, int delims_len )
{
    int i, j;
    bool has_delim;

    for ( i = 0; i < delims_len; i++ )
    {
        has_delim = false;
        for ( j = 0; str[j] != '\0' && !has_delim; j++ )
            if ( str[j] == str_delims[i] )
                has_delim = true;

        if ( !has_delim )
            return 0;
    }
    return 1;
}

bool is_delim ( char ch, char* str_delims, int delims_len )
{
    int i;

    for ( i = 0; i < delims_len; i++ )
        if ( str_delims[i] == ch )
            return true;

    return false;
}

void swap_words ( char* str, int word1_begin, int word1_len,
                  int word2_begin, int word2_len )
{
    char *tmp_word1 = nullptr,
          *tmp_word2 = nullptr,
           *tmp_btw   = nullptr;
    int i, btw_begin, btw_len, shift;

    tmp_word1 = (char*)malloc(word1_len*sizeof(char));
    tmp_word2 = (char*)malloc(word2_len*sizeof(char));

    for ( i = 0; i < word1_len; i++ )
    {
        tmp_word1[i] = str[word1_begin + i];
    }
    btw_begin = word1_begin + i;
    btw_len = word2_begin - btw_begin;
    tmp_btw = (char*)malloc(btw_len*sizeof(char));

    for ( i = 0; i < word2_len; i++ )
    {
        tmp_word2[i] = str[word2_begin + i];
    }

    for ( i = 0; i < btw_len; i++ )
    {
        tmp_btw[i] = str[btw_begin + i];
    }

    i = shift = word1_begin;
    while ( i < shift + word2_len )
    {
        str[i] = tmp_word2[i - shift];
        i++;
    }
    shift += word2_len;
    while ( i < shift + btw_len )
    {
        str[i] = tmp_btw[i - shift];
        i++;
    }
    shift += btw_len;
    while ( i < shift + word1_len )
    {
        str[i] = tmp_word1[i - shift];
        i++;
    }


    free(tmp_word1);
    free(tmp_word2);
    free(tmp_btw);
}
void get_max_word ( char* str, char* delims, int delims_len,
                    int start, int* index_ptr, int* len_ptr )
{
    int i, index, max_len, cur_len;

    max_len = 0;
    index = -1;
    cur_len = 0;
    for ( i = start; str[i] != '\0'; i++ )
    {
        if ( !is_delim ( str[i], delims, delims_len ) )
        {
            cur_len++;
            if ( str[i+1] == '\0' && cur_len >= max_len )
            {
                max_len = cur_len;
                index = i - max_len + 1;
                cur_len = 0;
            }
        }
        else
        {
            if ( cur_len >= max_len )
            {
                max_len = cur_len;
                index = i - max_len;
            }
            cur_len = 0;
        }
    }

    *index_ptr = index;
    *len_ptr = max_len;
}

void get_first_word ( char* str, char* delims, int delims_len, int start, int* index_ptr, int* len_ptr )
{
    int i, cur_len;
    cur_len = 0;
    for ( i = start; str[i] != '\0'; i++ )
    {
        if ( !is_delim ( str[i], delims, delims_len ) )
        {
            cur_len++;
        }
        if ( ( is_delim ( str[i], delims, delims_len ) ||
                str[i+1] == '\0' ) && cur_len != 0 )
        {
            *len_ptr = cur_len;
            *index_ptr = i - cur_len;
            *index_ptr += (str[i+1] == '\0') ? 1 : 0 ;
            return;
        }
    }
    *len_ptr = 0;
    *index_ptr = -1;
}
void del_str_from_arr ( char** str_arr, int str_arr_len, int index )
{
    int i;
    free(str_arr[index]);
    for (i = index; i < str_arr_len-1; i++)
    {
        str_arr[i] = str_arr[i+1];
    }
    str_arr[i] = nullptr;
}