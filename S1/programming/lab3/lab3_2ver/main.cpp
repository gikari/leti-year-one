#include <iostream>

using namespace std;

int main()
{
    int cn = 0;
    int ap, s = 0;
    int n, a;
    cout << "Input n: " << endl;
    cin >> n;
    while (cn < n)
    {
        cout << "Input a: ";
        cin >> a;
        if (a*ap < 0 || cn == 0 )
        {
            cn = 1;
            if (a < 0)
                s = a;
        }
        else
        {
            cn ++;
            if (a < 0)
                s += a;
        }
        ap = a;
    }

    cout << "Result: " << s;

    return 0;
}
