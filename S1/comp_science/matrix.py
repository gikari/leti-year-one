#!/usr/bin/python3
# -*- coding: utf-8 -*-

matrixA = []
matrixB = []
matrixC = []

print("C = AB")

while True:
    ma = int(input("Введите количество строк матрицы A: "))
    na = int(input("Введите количество столбцов матрицы A: "))

    mb = int(input("Введите количество строк матрицы B: "))
    nb = int(input("Введите количество столбцов матрицы B: "))

    if ma == nb and mb == na and ma > 0 and na > 0 and mb > 0 and nb > 0:
        break
    else:
        print("Нельзя умножать такие матрицы, повторите ввод")

for i in range(ma):
    matrixA.append([])
    for j in range(na):
        matrixA[i].append(
            int(input("Введите A[" + str(i) + "][" + str(j) + "]: "))
            )

print(matrixA)

for i in range(mb):
    matrixB.append([])
    for j in range(nb):
        matrixB[i].append(
            int(input("Введите B[" + str(i) + "][" + str(j) + "]: "))
            )

print(matrixB)

for i in range(ma):
    matrixC.append([])
    for j in range(nb):
        mlps = []
        for y in range(mb):
            mlps.append(matrixA[i][y]*matrixB[y][j])

        matrixC[i].append(sum(mlps))
print('\n')
print(matrixC)

"""
C[0][0] = A[0][0]*B[0][0] + A[0][1]*B[1][0] + A[1][2]*B[2][0]
C[0][1] = A[0][0]*B[0][1] + A[0][1]*B[1][1] + A[1][2]*B[2][1]
C[0][2] = A[0][0]*B[0][2] + A[0][1]*B[1][2] + A[1][2]*B[2][2]

C[1][0] = A[1][0]*B[0][0] + A[1][1]*B[1][0] + A[1][2]*B[2][0]
C[1][1] = A[1][0]*B[0][1] + A[1][1]*B[1][1] + A[1][2]*B[2][1]
C[1][2] = A[1][0]*B[0][2] + A[1][1]*B[1][2] + A[1][2]*B[2][2]

C[2][0] = A[2][0]*B[0][0] + A[1][1]*B[1][0] + A[1][2]*B[2][0]
C[2][1] = A[2][0]*B[0][1] + A[1][1]*B[1][1] + A[1][2]*B[2][1]
C[2][2] = A[2][0]*B[0][2] + A[1][1]*B[1][2] + A[1][2]*B[2][2]


"""
