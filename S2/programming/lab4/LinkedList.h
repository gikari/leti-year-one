/*
 * LinkedList.h
 *
 *  Created on: 21 апр. 2017 г.
 *      Author: svin-ru
 */

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_
namespace LinkedList
{

struct li
{
    int data;
    li* next;
    li* prev;
};

li* clear(li* head);
li* insert_element(li* head, int data);
li* input(li* head);
li* console_input(li* head);
li* file_input(li* head);

void output(li* head);
void console_output(li* head);
void file_output(li* head);

li* delete_from_n(li* head);
li* find_by_num(li * head, int start_num);
bool check_free_after(li* start, int delete_total);

}
#endif /* LINKEDLIST_H_ */
