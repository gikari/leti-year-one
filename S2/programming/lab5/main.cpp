/*
 * main.cpp
 *
 *  Created on: 18 апр. 2017 г.
 *      Author: svin-ru
 */

#include "main.h"
#include "LinkedList.h"
#include <iostream>
using namespace std;

int main()
{
    char choice;
    LinkedList::li* list_head = nullptr;
    do
    {
        print_help();
        cin >> choice;
        switch (choice)
        {
        case 'I':
            list_head = LinkedList::clear(list_head);
            list_head = LinkedList::input(list_head);
            break;
        case 'A':
            list_head = LinkedList::append_with_cond(list_head);
            break;
        case 'O':
            LinkedList::output(list_head);
            break;
        case 'Q':
            cout << "Выходим..." << endl;
            break;
        default:
            cout << "Повторите ввод!" << endl;
        }
    } while (choice != 'Q');
    list_head = LinkedList::clear(list_head);
    return 0;
}

void print_help()
{
    cout << "I - Ввести список" << endl;
    cout << "A - Добавить с условием" << endl;
    cout << "O - Вывести список" << endl;
    cout << "Q - Выход" << endl;
}
