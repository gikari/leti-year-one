/*
 * main.hpp
 *
 *  Created on: 17 мар. 2017 г.
 *      Author: svin-ru
 */

#ifndef MAIN_HPP_
#define MAIN_HPP_


struct li
{
    int data;
    li* next;
};

struct int_arr
{
    int * arr;
    int len;
};
int_arr input_arr();
li* input_list();

li * create_list(int_arr* elem_arr, li* source_head);
void sort_ints(int_arr * elem_arr);
li* add_elem(li * elem_to_add, li* dest_head);

void output_list(li* head);
void output_int_arr(int_arr *);

void delete_list(li* head);
void delete_int_arr(int_arr* p);


#endif /* MAIN_HPP_ */
