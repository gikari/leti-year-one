/*
 * main.cpp
 *
 *  Created on: 14 мар. 2017 г.
 *      Author: svin-ru
 */
#include <iostream>
#include "main.hpp"

using namespace std;

int main()
{
    li* head0 = nullptr;
    li* head_new = nullptr;
    int_arr elements_arr
    { nullptr, 0 };
    int act;

    do
    {
        cout << "1 - Ввести все данные" << endl
                << "2 - Ввести массив из номеров элементов" << endl
                << "3 - Ввести исходный список" << endl //
                << "4 - Сформировать список" << endl  //
                << "5 - Вывести список" << endl //
                << "6 - Вывести исходные данные" << endl //
                << "0 - Выход" << endl; //
        cin >> act;
        switch (act)
        {
        case 1:
            delete_list(head0);
            head0 = input_list();
            delete_int_arr(&elements_arr);
            elements_arr = input_arr();
            break;
        case 2:
            delete_int_arr(&elements_arr);
            elements_arr = input_arr();
            break;
        case 3:
            delete_list(head0);
            head0 = input_list();
            break;
        case 4:
            head_new = create_list(&elements_arr, head0);
            break;
        case 5:
            output_list(head_new);
            break;
        case 6:
            output_list(head0);
            output_int_arr(&elements_arr);
            break;
        case 0:
            cout << "Выходим...";
            break;
        default:
            cout << "Некорректный ввод! Повторите попытку." << endl;
        }
    } while (act != 0);

    delete_list(head0);
    head0 = nullptr;
    delete_list(head_new);
    head_new = nullptr;
    delete_int_arr(&elements_arr);
    return 0;
}

int_arr input_arr()
{
    int_arr arr
    { nullptr, 0 };
    do
    {
        cout << "Введите длину массива элементов: " << endl;
        cin >> arr.len;
    } while (arr.len <= 0);

    arr.arr = new int[arr.len];

    for (int i = 0; i < arr.len; i++)
    {
        cout << "Введите элемент массива №" << i << endl;
        cin >> arr.arr[i];
    }

    return arr;
}

li* input_list()
{
    li * head = nullptr;
    int len;
    do
    {
        cout << "Введите длину списка" << endl;
        cin >> len;
    } while (len < 0);
    if (len != 0)
    {
        head = new li;
        cout << "Введите инф. поле 1-го элемента" << endl;
        cin >> head->data;
        li* cur = head;
        for (int i = 1; i < len; i++)
        {
            cur->next = new li;
            cout << "Введите инф. поле " << i + 1 << "-го элемента" << endl;
            cin >> (cur->next)->data;
            cur = cur->next;
        }
        cur->next = nullptr;
    }
    return head;
}

li* create_list(int_arr * elem_arr, li* source_head)
{
    sort_ints(elem_arr);
    li* new_list = nullptr;

    int num_for_ins = 1;
    for (int i = 0; i < elem_arr->len; i++)
    {
        int arr_elem = elem_arr->arr[i];

        if (arr_elem < 1)
        {
            cout << "Элемента с номером " << arr_elem << " в списке нет."
                    << endl;
        }
        else
        {
            while (num_for_ins != arr_elem && source_head != nullptr)
            {
                source_head = source_head->next;
                num_for_ins++;
            }
            if (source_head != nullptr)
                new_list = add_elem(source_head, new_list);
            else
                cout << "Элемента с номером " << arr_elem << " в списке нет."
                        << endl;

        }
    }

    return new_list;
}

void output_list(li* head)
{
    while (head != nullptr)
    {
        cout << head->data << " -> ";
        head = head->next;
    }
    cout << endl;
}

void delete_list(li* head)
{
    while (head != nullptr)
    {
        li* for_del = head;
        head = head->next;
        delete for_del;
    }
}

void output_int_arr(int_arr * arr)
{
    for (int i = 0; i < arr->len; i++)
    {
        cout << arr->arr[i] << " ";
    }
    cout << endl;
}

void sort_ints(int_arr* elem_arr)
{
    for (int i = 0; i < elem_arr->len; i++)
    {
        for (int j = i + 1; j < elem_arr->len; j++)
        {
            if (elem_arr->arr[j] < elem_arr->arr[i])
            {
                int tmp = elem_arr->arr[j];
                elem_arr->arr[j] = elem_arr->arr[i];
                elem_arr->arr[i] = tmp;
            }
        }
    }

}

li* add_elem(li* elem_to_add, li* dest_head)
{
    if (dest_head == nullptr)
    {
        dest_head = new li;
        dest_head->data = elem_to_add->data;
        dest_head->next = nullptr;
    }
    else
    {
        li * last = dest_head;
        while (last->next != nullptr)
            last = last->next;
        last->next = new li;
        (last->next)->data = elem_to_add->data;
        (last->next)->next = nullptr;
    }
    return dest_head;
}

void delete_int_arr(int_arr* p)
{
    delete[] p->arr;
}

