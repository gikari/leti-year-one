#include <iostream>
#include <cstdio>
#include "main.hpp"

using namespace std;
int main()
{
    char choice;
    li* list_head = nullptr;
    int num_for_swap;
    int z;
    do
    {
        print_help();
        cin >> choice;
        switch (choice)
        {
        case 'I':
            list_head = input(list_head, &num_for_swap);
            break;
        case 'P':
            list_head = process(list_head, num_for_swap);
            break;
        case 'O':
            output(list_head);
            break;
        case 'E':
            cout << "Выходим..." << endl;
            break;
        case 'N':
            z = 0;
            cout << "Введите сколько раз вывести список: " << endl;
            cin >> z;
            output_n_times(list_head, z);
            break;
        default:
            cout << "Некорректный ввод. Повторите попытку." << endl;
        }
    } while (choice != 'E');
    list_head = delete_list(list_head);
    return 0;
}

void print_help()
{
    cout << "I - Ввести данные" << endl //
            << "P - Обработать данные" << endl //
            << "O - Вывести данные" << endl //
            << "E - Выход" << endl;
}

li* input(li* list_head, int* num_for_swap_ptr)
{
    char choice2;
    cout << "Существующие данные будут перезаписаны." << endl;
    cout << "Ввести из файла? (y/n/другой символ для отмены)" << endl;
    cin >> choice2;
    if (choice2 == 'y')
    {
        list_head = delete_list(list_head);
        list_head = file_input(list_head, num_for_swap_ptr);
    }
    else if (choice2 == 'n')
    {
        list_head = delete_list(list_head);
        list_head = console_input(list_head, num_for_swap_ptr);
    }
    else
        cout << "Отмена." << endl;
    return list_head;
}

li* file_input(li* list_head, int* num_for_swap_ptr)
{
    int element;
    FILE * i_file = fopen("config/input.txt", "r");
    if (i_file)
    {
        fscanf(i_file, "%d, ", num_for_swap_ptr);

        while (!feof(i_file))
        {
            fscanf(i_file, "%d ", &element);
            list_head = insert_element(list_head, element);
        }
    }
    else
        cout << "Ошибка открытия файла" << endl;
    fclose(i_file);

    return list_head;
}

li* console_input(li* list_head, int* num_for_swap_ptr)
{
    int len, element;
    do
    {
        cout << "Введите количество элементов в списке: " << endl;
        cin >> len;
    } while (len < 0);

    for (int i = 0; i < len; i++)
    {
        cout << "Введите следующее поле списка:" << endl;
        cin >> element;
        list_head = insert_element(list_head, element);
    }

    cout << "Введите номер в списке для перестановки со следующим:" << endl;
    cin >> (*num_for_swap_ptr);
    return list_head;
}

li* process(li* list_head, int num_for_swap)
{
    li* prev = get_previous(list_head, num_for_swap);
    if (prev)
        list_head = swap_next_with_next(list_head, prev);

    return list_head;
}

li* insert_element(li* list_head, int info_field)
{
    if (list_head)
    {
        li* new_element = new li;
        li* cur = list_head;
        new_element->data = info_field;
        while (cur->next != list_head)
            cur = cur->next;
        cur->next = new_element;
        new_element->next = list_head;
    }
    else
    {
        list_head = new li;
        list_head->data = info_field;
        list_head->next = list_head;
    }
    return list_head;
}

li* get_previous(li* list_head, int next_num)
{
    li* prev = nullptr;

    if (list_head)
    {
        li* cur = list_head;
        if (next_num != 1)
        {
            int i = 1;
            for (; i < next_num - 1 && cur->next != list_head; i++)
                cur = cur->next;
            if (i == next_num - 1)
                prev = cur;
            else
                cout << "Элемента с номером " << next_num << " в списке нет."
                        << endl;
        }
        else
        {
            while (cur->next != list_head)
                cur = cur->next;
            prev = cur;
        }
    }
    return prev;
}

li* swap_next_with_next(li* list_head, li* prev)
{
    li* first = prev->next;
    li* second = first->next;

    prev->next = second;
    first->next = second->next;
    second->next = first;

    if (list_head == first)
        list_head = second;
    else if (list_head == second)
        list_head = first;

    return list_head;
}

void output(li* list_head)
{
    char choice2;
    cout << "Существующие выходные данные в файле будут перезаписаны." << endl;
    cout << "Вывести в файл? (y/n/другой символ для отмены)" << endl;
    cin >> choice2;
    if (choice2 == 'y')
        file_output(list_head);
    else if (choice2 == 'n')
        console_output(list_head);
    else
        cout << "Отмена." << endl;
}

void file_output(li* list_head)
{
    FILE * o_file = fopen("logs/output.txt", "w");
    if (o_file)
    {
        if (list_head)
        {
            fprintf(o_file, "-0- -> ");
            li* cur = list_head;
            do
            {
                fprintf(o_file, "%d -> ", cur->data);
                cur = cur->next;
            } while (cur != list_head);
            fprintf(o_file, "-0-");
        }
        else
            fprintf(o_file, "Пусто.\n");
    }
    else
        cout << "Ошибка открытия файла." << endl;
    fclose(o_file);

}

void console_output(li* list_head)
{
    if (list_head)
    {
        cout << "-0- -> ";
        li* cur = list_head;
        do
        {
            cout << cur->data << " -> ";
            cur = cur->next;
        }
        while (cur != list_head);
        cout << "-0-" << endl;
    }
    else
        cout << "Пусто" << endl;
}

li* delete_list(li* list_head)
{
    if (list_head)
    {
        li* cur = list_head->next;
        while (cur != list_head)
        {
            li* next_to_del = cur->next;
            delete cur;
            cur = next_to_del;
        }
        delete cur;
    }
    return nullptr;
}

void output_n_times(li* list_head, int n)
{
    if (list_head)
    {
        cout << "-0- -> ";
        li* cur = list_head;
        for (int i = 0; i < n; i++)
        {
            do
            {
                cout << cur->data << " -> ";
                cur = cur->next;
            } while (cur != list_head);
        }
        cout << "-0-" << endl;
    }
    else
        cout << "Пусто" << endl;
}
