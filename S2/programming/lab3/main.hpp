/*
 * main.hpp
 *
 *  Created on: 28 мар. 2017 г.
 *      Author: svin-ru
 */

#ifndef MAIN_HPP_
#define MAIN_HPP_

struct li
{
    int data;
    li* next;
};

void print_help();
li* input(li* list_head, int* num_for_swap_ptr);
li* file_input(li* list_head, int* num_for_swap_ptr);
li* console_input(li* list_head, int* num_for_swap_ptr);

li* process(li* list_head, int num_for_swap);
li* insert_element(li* list_head, int info_field);
li* get_previous(li* list_head, int next_num);
li* swap_next_with_next(li* list_head, li* prev);

void output(li* list_head);
void file_output(li* list_head);
void console_output(li* list_head);

li* delete_list(li* list_head);

void output_n_times(li* list_head, int n);

#endif /* MAIN_HPP_ */
