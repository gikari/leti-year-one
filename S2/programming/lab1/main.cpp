/*
 * main.cpp
 *
 *  Created on: 23 февр. 2017 г.
 *      Author: svin-ru
 */
#include <iostream>
#include <malloc.h>
using namespace std;

struct something
{
    int a;
    char b;
    float c;
};

something * input_data(int * const len);
something * output_data(something * sth, int * const len, const int filter_min,
        const int filter_max, const int stright);
void print_struct(something*);

int main()
{
    something * struct_arr = nullptr;
    int len = -1;
    int action = -1, i;
    while (action != 0)
    {
        cout << "1 - Ввести данные" << endl;
        cout << "2 - Вывести по критерию с удалением" << endl;
        cout << "3 - Вывести весь массив" << endl;
        cout << "0 - Выход" << endl;
        cin >> action;
        switch (action)
        {
        case 1:
            if (len != -1)
                {
                cout << "Данные будут перезаписаны, вы уверены?"
                        " (1 - Да, 0 - Нет)";
                cin >> action;
                }
            if (action == 1)
                struct_arr = input_data(&len);
            else
                action = 1;
            break;
        case 2:
            if (len != -1)
            {
                int filter_min, filter_max, stright;
                cout << "Введите нижнюю границу для свойства 1 (int): ";
                cin >> filter_min;
                cout << "Введите верхнюю границу для свойства 1 (int): ";
                cin >> filter_max;
                do
                {
                    cout << "Вывод и удаление в прямом или обратном порядке? "
                            "(1 - Прямой, 0 - Обратный) " << endl;
                    cin >> stright;
                } while (stright != 1 && stright != 0);

                struct_arr = output_data(struct_arr, &len, filter_min,
                        filter_max, stright);
            }

            else
                cout << "Данные не введены" << endl;
            break;
        case 3:
            if (len != -1)
                for (i = 0; i < len; i++)
                {
                    cout << endl;
                    print_struct(struct_arr + i);
                }
            else
                cout << "Данные не введены" << endl;
            break;
        }
    }
    free(struct_arr);
    return 0;
}

something * input_data(int * const len)
{
    int i, length = *len;
    something * sth = nullptr;
    do
    {
        cout << "Введите длину массива структур: ";
        cin >> length;
    } while (length <= 0);
    sth = (something*) malloc(length * sizeof(something));
    if (sth == nullptr)
    {
        cout << "Недостаточно памяти";
        exit(-1);
    }
    for (i = 0; i < length; i++)
    {
        cout << "Структура " << i << endl;
        cout << "Введите свойство a (int): ";
        cin >> sth[i].a;
        cout << "Введите свойство b (char): ";
        cin >> sth[i].b;
        cout << "Введите свойство c (float): ";
        cin >> sth[i].c;
        cout << endl;
    }

    *len = length;
    return sth;
}

something * output_data(something * sth, int * const len, const int filter_min,
        const int filter_max, const int stright)
{
    int i, del_index = -1;
    int length = *len;


    if (stright == 1)
    {
        for (i = 0; i < length; i++)
        {
            if (sth[i].a >= filter_min && sth[i].a <= filter_max)
            {
                cout << endl;
                print_struct(sth + i);
                del_index = i;
            }
        }
    }
    else
    {
        for (i = length - 1; i >= 0; i--)
        {
            if (sth[i].a >= filter_min && sth[i].a <= filter_max)
            {
                cout << endl;
                print_struct(sth + i);
                del_index = i;
            }
        }
    }

    if (del_index == -1)
        return sth;

    length--;
    for (i = del_index; i < length; i++)
    {
        sth[i] = sth[i + 1];
    }

    sth = (something*) realloc(sth, length);
    *len = length;
    return sth;
}

void print_struct(something* sth)
{
    cout << "Структура: " << endl;
    cout << "a = " << sth->a << endl;
    cout << "b = " << sth->b << endl;
    cout << "c = " << sth->c << endl;
    cout << endl;
}
