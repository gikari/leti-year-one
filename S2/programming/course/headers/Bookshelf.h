#ifndef BOOKSHELF_H_
#define BOOKSHELF_H_

namespace Bookshelf
{
struct AuthorLi;
struct BookLi;
struct Author;
struct Book;

static AuthorLi* author_list;

struct Author
{
    char* full_name;
    int birth_date;
    char* desciplines;
    char* filename;
    BookLi* bibliography;
};

struct Book
{
    char* authors;
    char* title;
    char* publishing;
    int year;
    char* isbn;
};

void append();
void remove_item();
void edit();
void read_file();
void write_file();

void append_author();
void append_book();

struct BookLi
{
    Book* data;
    BookLi* next;
    BookLi* prev;
};

void append(Author* book_author, Book* element);
BookLi* get_last(BookLi* head);
void clear(BookLi* head);

struct AuthorLi
{
    Author * data;
    AuthorLi* next;
    AuthorLi* prev;
};

AuthorLi* append(AuthorLi* head, Author* element);
Author* find_author(char* author_str);

void console_output();

void clear(AuthorLi* head);

}

#endif /* BOOKSHELF_H_ */
