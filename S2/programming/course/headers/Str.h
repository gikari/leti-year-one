#ifndef STR_H_
#define STR_H_

namespace Str
{
char* input();
char* parse(char* str);
void append_char(char* str, char ch);
bool compare(char* str1, char* str2);
int strlen(char* str);

}

#endif /* STR_H_ */
