#ifndef MENU_H_
#define MENU_H_

namespace Menu
{

void init();
void print_help();
void handle_input(char choice);
void free_res();

}

#endif /* MENU_H_ */
