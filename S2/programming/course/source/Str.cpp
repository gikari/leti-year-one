/*
 * Str.cpp
 *
 *  Created on: 30 апр. 2017 г.
 *      Author: svin-ru
 */
#include "../headers/Str.h"
#include <iostream>
#include <malloc.h>

using namespace std;


namespace Str
{

char* input()
{
    char* string = nullptr;
    cin.ignore(10000, '\n');

    int k = 0;
    do
    {
        k++;
        string = (char*) realloc(string, k * sizeof(char));
        cin.get(string[k - 1]);
    } while (string[k - 1] != '\n');
    string[k - 1] = '\0';

    return string;
}

char* parse(char* str) {
	char* parsed = nullptr;
	// from start to ','
	int i = 0;
	while (str[i] != ',' || str[i] != '\0') {
		append_char(parsed, str[i]);
		i++;
	}
	return parsed;
}

void append_char(char* str, char ch) {
	int len = strlen(str);
	str = (char*) realloc(str, len + 1);
	str[len] = ch;
	str[len + 1] = '\0';
}

int strlen(char* str) {
	int i = 0;
	while (str[i++] != '\0')
		;
	return i;
}

bool compare(char* str1, char* str2) {
	if (strlen(str1) == strlen(str2)) {
		int i = 0;
		while (str1[i] != '\0') {
			if (str1[i] != str2[i])
				return false;
			i++;
		}
		return true;
	}
	return false;
}

}
