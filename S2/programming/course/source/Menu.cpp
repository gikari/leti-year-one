#include "../headers/Bookshelf.h"
#include "../headers/Menu.h"
#include <iostream>

using namespace Bookshelf;
using namespace std;

namespace Menu
{

void init()
{
    Bookshelf::author_list = nullptr;
    char choice = '_';
    //Bookshelf::read_file();
    do
    {
        print_help();
        cin >> choice;
        handle_input(choice);
    }
    while (choice != 'Q');
    free_res();
}

void print_help()
{
    cout << "A - Добавить элемент в хранилище\n"
            "D - Удалить элемент из хранилища\n"
            "E - Редактировать элемент в хранилище\n"
            "R - Прочитать библиографию из файла\n"
            "W - Записать библиографию в файл\n"
            "Q - Выход\n";
}

void handle_input(char choice)
{
    switch (choice)
    {
    case 'A':
        Bookshelf::append();
        break;
    case 'D':
        Bookshelf::remove_item();
        break;
    case 'E':
        Bookshelf::edit();
        break;
    case 'R':
        Bookshelf::read_file();
        break;
    case 'W':
        Bookshelf::write_file();
        break;
    case 'Q':
        cout << "Выходим...\n";
        break;
    default:
        cout << "Повторите ввод!\n";
    }
}

void free_res()
{
    clear(Bookshelf::author_list);
}

}
