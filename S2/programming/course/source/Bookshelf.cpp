/*
 * Bookshelf.cpp
 *
 *  Created on: 30 апр. 2017 г.
 *      Author: svin-ru
 */

#include "../headers/Bookshelf.h"

#include <iostream>

#include "../headers/Str.h"

using namespace std;

namespace Bookshelf
{

void append()
{
    char choice = '_';
    do
    {
        cout << "A - Добавить Автора\n"
                "B - Добавить Книгу\n"
                "Q - Прервать\n";
        cin >> choice;
        switch (choice)
        {
        case 'A':
            append_author();
            break;
        case 'B':
            append_book();
            break;
        case 'Q':
            break;
        default:
            cout << "Повторите ввод!\n";
        }
    }
    while (choice != 'A' && choice != 'B' && choice != 'Q');
}

void remove_item()
{
}

void edit()
{
}

void read_file()
{
}

void write_file()
{
}

void append_author()
{
    Author* new_author = new Author;
    cout << "Введите Фамилию.И.О. автора:\n";
    new_author->full_name = Str::input();
    cout << "Введите день рождения автора:\n";
    cin >> new_author->birth_date;
    cout << "Введите дисциплины автора (через запятую):\n";
    new_author->desciplines = Str::input();
    cout << "Введите имя файла для хранения книг автора:\n";
    new_author->filename = Str::input();
    new_author->bibliography = nullptr;

    append(author_list, new_author);
}

void append_book()
{
    Book* new_book = new Book;
    cout << "Введите авторов книги через запятую:\n";
    new_book->authors = Str::input();
    cout << "Введите название книги:\n";
    new_book->title = Str::input();
    cout << "Введите издательство книги:\n";
    new_book->publishing = Str::input();
    cout << "Введите год издательство:\n";
    cin >> new_book->year;
    cout << "Введите ISBN:\n";
    new_book->isbn = Str::input();

    Author* book_author = find_author(new_book->authors);
	if (book_author)
		append(book_author, new_book);

}

AuthorLi* append(AuthorLi* head, Author* element)
{
    if (head)
    {
        AuthorLi* new_element = new AuthorLi;
        new_element->data = element;
        new_element->next = nullptr;

        AuthorLi* cur = head;
        while (cur->next != nullptr)
            cur = cur->next;
        cur->next = new_element;
        new_element->prev = cur;
    }
    else
    {
        head = new AuthorLi;
        head->data = element;
        head->next = nullptr;
        head->prev = nullptr;
    }
    return head;
}

void append(Author* author, Book* element)
{
	// TODO append new book to author's bibliography list
	BookLi* new_element = new BookLi;
	new_element->data = element;
	new_element->next = nullptr;
	BookLi* last_in_list = get_last(author->bibliography);
	last_in_list->next = new_element;
	new_element->prev = last_in_list;

}

void clear(BookLi* head)
{
	while (head != nullptr)
	{
        BookLi* next = head->next;
        delete head;
        head = next;
    }
}

Author* find_author(char* author_str)
{
	char* parsed_authors_str = Str::parse(author_str);
	// for each in authors_list
	AuthorLi * cur = author_list;
	while (cur) {
		if (Str::compare(parsed_authors_str, (cur->data)->full_name))
			return cur->data;
		cur = cur->next;
	}
    return nullptr;
}

void console_output() {

}

BookLi* get_last(BookLi* head) {
	while (head && head->next)
		head = head->next;
	return head;
}

void clear(AuthorLi* head)
{
    while (head != nullptr)
    {
        AuthorLi* next = head->next;
        delete head;
        head = next;
    }
}

}
