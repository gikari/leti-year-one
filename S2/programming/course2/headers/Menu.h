#ifndef MENU_H_
#define MENU_H_
#include <string>

namespace Menu
{

void init();
void print_help();
void handle_input(const std::string &choice);
void free_res();

}

#endif /* MENU_H_ */
