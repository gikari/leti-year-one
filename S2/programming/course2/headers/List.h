#ifndef LIST_H
#define LIST_H

#include <string>
#include <vector>
#include "Bookshelf.h"

namespace Bookshelf
{
    struct Author;
    struct Book;
}

namespace List
{
    struct AuthorLi
    {
        Bookshelf::Author * data;
        AuthorLi* next;
        AuthorLi* prev;
    };
    struct BookLi
    {
        Bookshelf::Book* data;
        BookLi* next;
        BookLi* prev;
    };

    AuthorLi* append_author_to_list(AuthorLi* head, Bookshelf::Author* element);
    BookLi *append_book_to_list(BookLi* head, Bookshelf::Book* element);

    AuthorLi *remove_author_from_list(AuthorLi *head, Bookshelf::Author* author);
    BookLi *remove_book_from_list(BookLi *head, Bookshelf::Book* book);

    AuthorLi *clear_author_list(AuthorLi* head);
    BookLi *clear_book_list(BookLi *book_li);

    AuthorLi *get_li_from_author_struct(Bookshelf::Author* author);
    BookLi *get_li_from_book_struct(Bookshelf::Book* book);


    std::vector<Bookshelf::Author*> get_authors_by_name(const std::string &property);
    std::vector<Bookshelf::Author*> get_authors_by_date(const std::string &property);
    std::vector<Bookshelf::Author*> get_authors_by_descipline(const std::string &property);
    std::vector<Bookshelf::Author*> get_authors_by_period(int min, int max);

    std::vector<Bookshelf::Book*> get_books_by_author(const std::string &property);
    std::vector<Bookshelf::Book*> get_books_by_title(const std::string &property);
    std::vector<Bookshelf::Book*> get_books_by_publishing(const std::string &property);
    std::vector<Bookshelf::Book*> get_books_by_year(const std::string &property);
    std::vector<Bookshelf::Book*> get_books_by_isbn(const std::string  &property);
    std::vector<Bookshelf::Book*> get_books_by_period(int min, int max);

    void output_authors(AuthorLi* list);
    void output_books(BookLi *list);

    AuthorLi *swap_authors(AuthorLi *head, AuthorLi *first, AuthorLi *second);
    BookLi *swap_books(BookLi *head, BookLi *first, BookLi *second);
}

#endif // LIST_H
