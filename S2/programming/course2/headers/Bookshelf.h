#ifndef BOOKSHELF_H_
#define BOOKSHELF_H_

#include "List.h"
#include <string>

namespace List
{
    struct AuthorLi;
    struct BookLi;
}

namespace Bookshelf
{

struct Book
{
    std::string authors;
    std::string title;
    std::string publishing;
    int year;
    std::string isbn;
};
struct Author
{
    std::string full_name;
    int birth_date;
    std::string desciplines;
    std::string filename;
    List::BookLi* bibliography;
};

extern List::AuthorLi* author_list;

void append_new_item_to_repo();
void remove_item_from_repo();
void edit_item_in_repo();
void read_repo_from_file();
void write_repo_to_file();

void append_new_author_to_repo();
void append_new_book_to_repo();
void remove_book_from_repo();
void remove_author_from_repo();
void edit_author_in_repo();
void edit_book_in_repo();

std::vector<Book*> get_books_from_repo();
std::vector<Author*> get_authors_from_repo();

void sort_lists_in_repo();
void output_with_condition();
void output_within_the_range();

void sort_authors_in_repo();
void sort_books_in_repo();

void output_authors_with_condition();
void output_books_with_condition();

void output_authors_within_the_range();
void output_books_within_the_range();

}

namespace Str
{
    std::vector<std::string> split (const std::string &str, const char delim);
}

#endif /* BOOKSHELF_H_ */
