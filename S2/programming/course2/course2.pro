TEMPLATE = app
CONFIG += console c++11
CONFIG += c++14
QMAKE_CXXFLAGS += -std=c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    source/Bookshelf.cpp \
    source/Menu.cpp \
    source/List.cpp

HEADERS += \
    headers/Bookshelf.h \
    headers/Menu.h \
    headers/List.h
