#include "../headers/Bookshelf.h"
#include "../headers/Menu.h"
#include <iostream>
#include <string>

using namespace Bookshelf;
using namespace std;

namespace Menu
{

void init()
{
    Bookshelf::author_list = nullptr;
    std::string choice {"_"};
    Bookshelf::read_repo_from_file();
    do
    {
        print_help();
        std::getline(std::cin, choice);
        //system("clear");
        handle_input(choice);
    }
    while (choice.front() != 'Q');
    free_res();
}

void print_help()
{
    cout << "A - Добавить элемент в хранилище\n"
            "D - Удалить элемент из хранилища\n"
            "E - Редактировать элемент в хранилище\n"
            " --- \n"
            "S - Сортировать список по возрастанию\n"
            "G - Показать структуры в хранилище с определенным значением\n"
            "B - Показать струкруты, чей год находится в заданном диапазоне\n"
            " --- \n"
            "R - Прочитать библиографию из файла\n"
            "W - Записать библиографию в файл\n"
            "Q - Выход\n";
}

void handle_input(const std::string &choice)
{
    switch (choice.front())
    {
    case 'A':
        Bookshelf::append_new_item_to_repo();
        break;
    case 'D':
        Bookshelf::remove_item_from_repo();
        break;
    case 'E':
        Bookshelf::edit_item_in_repo();
        break;
    case 'R':
        Bookshelf::read_repo_from_file();
        break;
    case 'W':
        Bookshelf::write_repo_to_file();
        break;
    case 'S':
        Bookshelf::sort_lists_in_repo();
        break;
    case 'G':
        Bookshelf::output_with_condition();
        break;
    case 'B':
        Bookshelf::output_within_the_range();
        break;
    case 'Q':
        cout << "Выходим...\n";
        break;
    default:
        cout << "Повторите ввод!\n";
    }
}

void free_res()
{
    clear_author_list(Bookshelf::author_list);
}

}
