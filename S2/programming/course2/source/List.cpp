#include "../headers/List.h"
#include "../headers/Bookshelf.h"
#include <iostream>
#include <vector>

using namespace std;
using namespace Bookshelf;

namespace List
{

AuthorLi *append_author_to_list(AuthorLi* head, Author* element)
{
    if (head)
    {
        AuthorLi* new_element = new AuthorLi;
        new_element->data = element;
        new_element->next = nullptr;

        AuthorLi* cur = head;
        while (cur->next != nullptr)
            cur = cur->next;
        cur->next = new_element;
        new_element->prev = cur;
    }
    else
    {
        head = new AuthorLi;
        head->data = element;
        head->next = nullptr;
        head->prev = nullptr;
    }
    return head;
}
BookLi *append_book_to_list(BookLi *head, Book *element)
{
    if (head)
    {
        BookLi* new_element = new BookLi;
        new_element->data = element;
        new_element->next = nullptr;

        BookLi* cur = head;
        while (cur->next != nullptr)
            cur = cur->next;
        cur->next = new_element;
        new_element->prev = cur;
    }
    else
    {
        head = new BookLi;
        head->data = element;
        head->next = nullptr;
        head->prev = nullptr;
    }
    return head;
}

BookLi *clear_book_list(BookLi* head)
{
    while (head != nullptr)
    {
        BookLi* next = head->next;
        delete head;
        head = next;
    }
    return nullptr;
}
AuthorLi *clear_author_list(AuthorLi* head)
{
    while (head != nullptr)
    {
        AuthorLi* next = head->next;
        delete head;
        head = next;
    }
    return nullptr;
}

AuthorLi *remove_author_from_list(AuthorLi* head, Author *author)
{
    AuthorLi* author_li = get_li_from_author_struct(author);
    if (!author_li)
        return head;

    if (author_li->prev)
        author_li->prev->next = author_li->next;
    else
        head = author_li->next;
    if (author_li->next)
        author_li->next->prev = author_li->prev;

    delete author;
    delete author_li;
    return head;
}
BookLi *remove_book_from_list(BookLi *head, Book *book)
{
    BookLi *book_li = get_li_from_book_struct(book);
    if (!book_li)
        return head;

    if (book_li->prev)
        book_li->prev->next = book_li->next;
    else
        head = book_li->next;

    if (book_li->next)
        book_li->next->prev = book_li->prev;

    delete book;
    delete book_li;
    return head;
}

AuthorLi *get_li_from_author_struct(Author *author)
{
    AuthorLi* cur_author = Bookshelf::author_list;
    while (cur_author)
    {
        if (cur_author->data == author)
            return cur_author;

        cur_author = cur_author->next;
    }
    return nullptr;
}
BookLi *get_li_from_book_struct(Book *book)
{
    AuthorLi* cur_author = Bookshelf::author_list;
    while (cur_author)
    {
        BookLi *cur_book = (cur_author->data)->bibliography;
        while (cur_book)
        {
            if (cur_book->data == book)
                return cur_book;
            cur_book = cur_book->next;
        }

        cur_author = cur_author->next;
    }
    return nullptr;
}

std::vector<Book*> get_books_by_author(const std::string & property)
{
    std::vector <Book*> books {};
    AuthorLi* cur = Bookshelf::author_list;
    while (cur)
    {
        BookLi *cur_book = (cur->data)->bibliography;
        while (cur_book)
        {
            if (cur_book->data->authors.find(property) != std::string::npos)
                books.push_back(cur_book->data);

            cur_book = cur_book->next;
        }

        cur = cur->next;
    }
    return books;
}
std::vector<Book*> get_books_by_title(const std::string & property)
{
    std::vector <Book*> books {};
    AuthorLi* cur = Bookshelf::author_list;
    while (cur)
    {
        BookLi *cur_book = (cur->data)->bibliography;
        while (cur_book)
        {
            if (property.compare(cur_book->data->title) == 0)
                books.push_back(cur_book->data);

            cur_book = cur_book->next;
        }

        cur = cur->next;
    }
    return books;
}
std::vector<Book*> get_books_by_publishing(const std::string & property)
{
    std::vector <Book*> books {};
    AuthorLi* cur = Bookshelf::author_list;
    while (cur)
    {
        BookLi *cur_book = (cur->data)->bibliography;
        while (cur_book)
        {
            if (property.compare(cur_book->data->publishing) == 0)
                books.push_back(cur_book->data);

            cur_book = cur_book->next;
        }

        cur = cur->next;
    }
    return books;
}
std::vector<Book*> get_books_by_year(const std::string &property)
{
    std::vector <Book*> books {};
    int year = std::stoi(property);
    AuthorLi* cur = Bookshelf::author_list;
    while (cur)
    {
        BookLi *cur_book = (cur->data)->bibliography;
        while (cur_book)
        {
            if ((cur_book->data)->year == year)
                books.push_back(cur_book->data);

            cur_book = cur_book->next;
        }

        cur = cur->next;
    }
    return books;
}
std::vector<Book*> get_books_by_isbn(const std::string &property)
{
    std::vector <Book*> books {};
    AuthorLi* cur = Bookshelf::author_list;
    while (cur)
    {
        BookLi *cur_book = (cur->data)->bibliography;
        while (cur_book)
        {
            if (property.compare(cur_book->data->isbn) == 0)
                books.push_back(cur_book->data);

            cur_book = cur_book->next;
        }

        cur = cur->next;
    }
    return books;
}
std::vector<Book*> get_books_by_period(int min, int max)
{
    std::vector <Book*> books {};
    AuthorLi* cur = Bookshelf::author_list;
    while (cur)
    {
        BookLi *cur_book = (cur->data)->bibliography;
        while (cur_book)
        {
            if (cur_book->data->year >= min && cur_book->data->year <= max)
                books.push_back(cur_book->data);

            cur_book = cur_book->next;
        }

        cur = cur->next;
    }
    return books;
}

std::vector<Author*> get_authors_by_name(const std::string &property)
{
    std::vector <Author*> authors{};
    AuthorLi* cur = Bookshelf::author_list;
    while (cur)
    {
        if (property.compare(cur->data->full_name) == 0)
            authors.push_back(cur->data);

        cur = cur->next;
    }
    return authors;
}
std::vector<Author*> get_authors_by_date(const std::string &property)
{
    std::vector <Author*> authors{};
    AuthorLi* cur = Bookshelf::author_list;
    int date = std::stoi(property);
    while (cur)
    {
        if ((cur->data)->birth_date == date)
            authors.push_back(cur->data);

        cur = cur->next;
    }
    return authors;
}
std::vector<Author*> get_authors_by_descipline(const std::string &property)
{
    std::vector<Author*> authors{};
    AuthorLi* cur = Bookshelf::author_list;
    while (cur)
    {
        if (cur->data->desciplines.find(property) != std::string::npos)
            authors.push_back(cur->data);

        cur = cur->next;
    }
    return authors;
}
std::vector <Author*> get_authors_by_period(int min, int max)
{
    std::vector <Author*> authors {};
    AuthorLi* cur = Bookshelf::author_list;
    while (cur)
    {
        if (cur->data->birth_date >= min && cur->data->birth_date <= max)
            authors.push_back(cur->data);

        cur = cur->next;
    }
    return authors;
}

void output_authors(AuthorLi *list)
{
    cout << "{\n";
    while (list != nullptr)
    {
        cout << "{\n";
        cout << "  " << list->data->full_name << endl;
        cout << "  " << list->data->birth_date << endl;
        cout << "  " << list->data->desciplines << endl;
        cout << "  " << list->data->filename << endl;
        cout << "},\n";

        list = list->next;
    }
    cout << "},\n";
}
void output_books(BookLi *list)
{
    cout << "{\n";
    while (list != nullptr)
    {
        cout << "{\n";
        cout << "  " << list->data->authors<< endl;
        cout << "  " << list->data->title << endl;
        cout << "  " << list->data->publishing << endl;
        cout << "  " << list->data->year<< endl;
        cout << "  " << list->data->isbn<< endl;
        cout << "},\n";
        list = list->next;
    }
    cout << "},\n";
}

AuthorLi *swap_authors(AuthorLi *head, AuthorLi *first, AuthorLi *second)
{
    if (first->next) first->next->prev = second;
    if (second->next) second->next->prev = first;
    auto tmp = first->next;
    first->next = second->next;
    second->next = tmp;

    if (first->prev) first->prev->next = second;
    if (second->prev) second->prev->next = first;
    tmp = first->prev;
    first->prev = second->prev;
    second->prev = tmp;

    if (first == head)
        head = second;

    return head;
}

BookLi *swap_books(BookLi *head, BookLi *first, BookLi *second)
{
    if (first->next) first->next->prev = second;
    if (second->next) second->next->prev = first;
    auto tmp = first->next;
    first->next = second->next;
    second->next = tmp;

    if (first->prev) first->prev->next = second;
    if (second->prev) second->prev->next = first;
    tmp = first->prev;
    first->prev = second->prev;
    second->prev = tmp;

    if (first == head)
        head = second;

    return head;
}


}
