/*
 * Bookshelf.cpp
 *
 *  Created on: 30 апр. 2017 г.
 *      Author: svin-ru
 */

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include "../headers/Bookshelf.h"
#include "../headers/List.h"

using namespace std;
using namespace List;

namespace Bookshelf
{
List::AuthorLi* author_list = nullptr;
const std::string authors_filename = "authors.json";

void append_new_item_to_repo()
{
    std::string choice {"_"};
    do
    {
        cout << "A - Добавить Автора\n"
                "B - Добавить Книгу\n"
                "Q - Прервать\n";
        std::getline(std::cin, choice);
        switch (choice.front())
        {
        case 'A':
            append_new_author_to_repo();
            break;
        case 'B':
            append_new_book_to_repo();
            break;
        case 'Q':
            break;
        default:
            cout << "Повторите ввод!\n";
        }
    }
    while (choice.front() != 'A' && choice.front() != 'B' && choice.front() != 'Q');
}

void remove_item_from_repo()
{
    std::string choice{"_"};
    do
    {
        cout << "A - Удалить Автора\n"
                "B - Удалить Книгу\n"
                "Q - Прервать\n";
        std::getline(std::cin, choice);
        switch (choice.front())
        {
        case 'A':
            remove_author_from_repo();
            break;
        case 'B':
            remove_book_from_repo();
            break;
        case 'Q':
            break;
        default:
            cout << "Повторите ввод!\n";
        }
    }
    while (choice.front() != 'A' && choice.front() != 'B' && choice.front() != 'Q');
}

void edit_item_in_repo()
{
    std::string choice {"_"};
    do
    {
        cout << "A - Редактировать Автора\n"
                "B - Редактировать Книгу\n"
                "Q - Прервать\n";
        std::getline(std::cin, choice);
        switch (choice.front())
        {
        case 'A':
            edit_author_in_repo();
            break;
        case 'B':
            edit_book_in_repo();
            break;
        case 'Q':
            break;
        default:
            cout << "Повторите ввод!\n";
        }
    }
    while (choice.front() != 'Q' && choice.front() != 'A' && choice.front() != 'B');
}

void read_repo_from_file()
{
    author_list = clear_author_list(author_list);
    std::ifstream a_file(authors_filename, ios_base::in);

    std::string rstr;
    std::getline(a_file, rstr); // {
    while (!a_file.eof() && a_file.peek() > 0)
    {
        std::getline(a_file, rstr); // {
        if (rstr != "  {") break;
        Author* new_author = new Author;

        std::getline(a_file, rstr); // full_name
        rstr = rstr.substr(rstr.find(':'), rstr.find_last_of(',')); // Ищем значение
        rstr = rstr.substr(2, rstr.size()-4); // Избавляемся от кавычек (')
        new_author->full_name = rstr;

        std::getline(a_file, rstr); // birth_date
        rstr = rstr.substr(rstr.find(':'), rstr.find_last_of(',')); // Ищем значение
        rstr = rstr.substr(1, rstr.size()-2); // Избавляемся от кавычек (')
        new_author->birth_date = std::stoi(rstr);

        std::getline(a_file, rstr); // desciplines
        rstr = rstr.substr(rstr.find(':'), rstr.find_last_of(',')); // Ищем значение
        rstr = rstr.substr(2, rstr.size()-4); // Избавляемся от кавычек (')
        new_author->desciplines = rstr;

        std::getline(a_file, rstr); // filename
        rstr = rstr.substr(rstr.find(':'), rstr.find_last_of(',')); // Ищем значение
        rstr = rstr.substr(2, rstr.size()-4); // Избавляемся от кавычек (')
        new_author->filename = rstr;

        new_author->bibliography = nullptr;
        std::getline(a_file, rstr); // }
        author_list = append_author_to_list(author_list, new_author);


        std::ifstream book_file{new_author->filename, ios_base::in};
        std::getline(book_file, rstr); // {
        while (!book_file.eof() && book_file.peek() > 0)
        {
            std::getline(book_file, rstr); // {
            if (rstr != "  {") break;
            Book* new_book = new Book;

            std::getline(book_file, rstr); // authors
            rstr = rstr.substr(rstr.find(':'), rstr.find_last_of(',')); // Ищем значение
            rstr = rstr.substr(2, rstr.size()-4); // Избавляемся от кавычек (') и ,
            new_book->authors = rstr;

            std::getline(book_file, rstr); // title
            rstr = rstr.substr(rstr.find(':'), rstr.find_last_of(',')); // Ищем значение
            rstr = rstr.substr(2, rstr.size()-4);
            new_book->title = rstr;

            std::getline(book_file, rstr); // publishing
            rstr = rstr.substr(rstr.find(':'), rstr.find_last_of(',')); // Ищем значение
            rstr = rstr.substr(2, rstr.size()-4);
            new_book->publishing = rstr;

            std::getline(book_file, rstr); // year
            rstr = rstr.substr(rstr.find(':'), rstr.find_last_of(',')); // Ищем значение
            rstr = rstr.substr(1, rstr.size()-2);
            new_book->year = std::stoi(rstr);

            std::getline(book_file, rstr); // isbn
            rstr = rstr.substr(rstr.find(':'), rstr.find_last_of(',')); // Ищем значение
            rstr = rstr.substr(2, rstr.size()-4);
            new_book->isbn = rstr;

            std::getline(book_file, rstr); // }
            new_author->bibliography = append_book_to_list(new_author->bibliography, new_book);
        }
        book_file.close();

    }
    a_file.close();
}

void write_repo_to_file()
{
    std::fstream a_file(authors_filename, ios_base::out);

    a_file << "{\n";
    auto cur = author_list;
    while (cur)
    {
        a_file << "  {\n";

        a_file << "    'full_name':'" << cur->data->full_name << "',\n";
        a_file << "    'birth_date':" << cur->data->birth_date << ",\n";
        a_file << "    'desciplines':'" << cur->data->desciplines << "',\n";
        a_file << "    'filename':'" << cur->data->filename << "',\n";

        a_file << "  },\n";

        std::fstream b_file(cur->data->filename, ios_base::out);
        auto cur_book = cur->data->bibliography;
        b_file << "{\n";
        while (cur_book)
        {
            b_file << "  {\n";

            b_file << "    'authors':'" << cur_book->data->authors << "',\n";
            b_file << "    'title':'" << cur_book->data->title << "',\n";
            b_file << "    'publishing':'" << cur_book->data->publishing << "',\n";
            b_file << "    'year':" << cur_book->data->year << ",\n";
            b_file << "    'isbn':'" << cur_book->data->isbn << "',\n";

            b_file << "  },\n";

            cur_book = cur_book->next;
        }
        b_file << "},\n";
        b_file.close();

        cur = cur->next;
    }
    a_file << "},\n";
    a_file.close();
}


void append_new_author_to_repo()
{
    Author* new_author = new Author;
    cout << "Введите Фамилию.И.О. автора: ";
    std::getline(std::cin, new_author->full_name);
    cout << "Введите день рождения автора: ";
    std::string tmp{"_"};
    std::getline(std::cin, tmp);
    new_author->birth_date = stoi(tmp);
    cout << "Введите дисциплины автора (через запятую): ";
    std::getline(std::cin, new_author->desciplines);
    cout << "Введите имя файла для хранения книг автора: ";
    std::getline(std::cin, new_author->filename);
    new_author->bibliography = nullptr;

    author_list = append_author_to_list(author_list, new_author);
}
void append_new_book_to_repo()
{
    Book* new_book = new Book;
    cout << "Введите авторов книги через запятую: ";
    std::getline(std::cin, new_book->authors);
    cout << "Введите название книги: ";
    std::getline(std::cin, new_book->title);
    cout << "Введите издательство книги: ";
    std::getline(std::cin, new_book->publishing);
    cout << "Введите год издательства: ";
    std::string tmp{"_"};
    std::getline(std::cin, tmp);
    new_book->year = stoi(tmp);
    cout << "Введите ISBN: ";
    std::getline(std::cin, new_book->isbn);

    std::vector<std::string> authors = Str::split(new_book->authors, ',');
    for (auto author : authors)
    {
        std::vector<Author*> book_authors = List::get_authors_by_name(author);
        if (book_authors.empty())
        {
            cout << "Одного из авторов не существует в хранилище! "
                    "Добавьте его пожалуйста, прежде чем добавить новую книгу.\n";
            delete new_book;
            return;
        }
        book_authors[0]->bibliography = List::append_book_to_list(book_authors[0]->bibliography, new_book);
    }
}

void remove_author_from_repo()
{
    std::vector <Author*> authors = get_authors_from_repo();
    if (!authors.empty())
    {
        auto author = authors[0];
        List::clear_book_list(author->bibliography);
        author_list = remove_author_from_list(author_list, author);
    }
}
void remove_book_from_repo()
{
    std::vector<Book*> books = get_books_from_repo();
    for (Book *book : books)
    {
        std::vector<std::string> authors_strs = Str::split(book->authors, ',');
        for (auto author_str : authors_strs)
        {
            std::vector<Author*> book_authors = List::get_authors_by_name(author_str);
            for (auto book_author : book_authors)
            {
                book_author->bibliography = remove_book_from_list(book_author->bibliography, book);
            }
        }
    }
}

void edit_book_in_repo()
{
    std::vector <Book*> books = get_books_from_repo();
    if (books.empty())
    {
        cout << "Книга не найдена!\n";
        return;
    }
    Book *book = books[0];
    std::string prop{};
    std::string choice {"_"};
    bool want_exit{true};
    do
    {
        cout << "Какое свойство вы хотите изменить?\n"
                "T - Название\n"
                "P - Издательство\n"
                "Y - Год издания\n"
                "I - ISBN\n";
        std::getline(std::cin, choice);
        if (choice.front() == 'Q')
            break;
        cout << "Введите новое значение свойства: ";
        std::getline(std::cin, prop);
        switch (choice.front())
        {
        case 'T':
            book->title = prop;
            break;
        case 'P':
            book->publishing = prop;
            break;
        case 'Y':
            book->year = std::stoi(prop);
            break;
        case 'I':
            book->isbn = prop;
            break;
        case 'Q':
            break;
        default:
            cout << "Повторите ввод!\n";
        }
    }
    while (!want_exit);
}
void edit_author_in_repo()
{
    std::vector <Author *> authors = get_authors_from_repo();
    if (authors.empty())
    {
        cout << "Автор не найден!\n";
        return;
    }
    Author *author = authors[0];
    std::string prop{};
    std::string choice {"_"};
    bool want_exit{true};
    do
    {
        cout << "Какое свойство вы хотите изменить?\n"
                "N - ФИО автора\n"
                "B - Дату рождения\n"
                "D - Дисциплины автора\n"
                "F - Имя файла\n"
                "Q - Прервать\n";
        std::getline(std::cin, choice);
        if (choice.front() == 'Q')
            break;
        cout << "Введите новое значение свойства: ";
        std::getline(std::cin, prop);
        BookLi *cur_book{};
        switch (choice.front())
        {
        case 'N':
            cur_book = author->bibliography;
            while (cur_book)
            {
                cur_book->data->authors = prop;
                cur_book = cur_book->next;
            }
            author->full_name = prop;
            break;
        case 'B':
            author->full_name = std::stoi(prop);
            break;
        case 'D':
            author->desciplines = prop;
            break;
        case 'F':
            author->filename = prop;
            break;
        case 'Q':
            break;
        default:
            want_exit = false;
            cout << "Повторите ввод!\n";
        }
    }
    while (!want_exit);
}

std::vector<Book*> get_books_from_repo()
{
    std::vector<Book*> books{};
    std::string choice {"_"};
    std::string property{};
    bool want_exit {true};
    do
    {
        cout << "A - Выбрать по автору\n"
                "T - Выбрать по названию\n"
                "P - Выбрать по издательству\n"
                "Y - Выбрать по году издательства\n"
                "I - Выбрать по ISBN\n"
                "Q - Прервать\n";
        std::getline(std::cin, choice);
        if (choice.front() == 'Q')
            break;
        cout << "Введите значение этого поля: ";
        std::getline(std::cin, property);
        switch (choice.front())
        {
        case 'A':
            books = List::get_books_by_author(property);
            break;
        case 'T':
            books = List::get_books_by_title(property);
            break;
        case 'P':
            books = List::get_books_by_publishing(property);
            break;
        case 'Y':
            books = List::get_books_by_year(property);
            break;
        case 'I':
            books = List::get_books_by_isbn(property);
            break;
        default:
            cout << "Повторите ввод!\n";
        }
    }
    while (!want_exit);
    return books;
}
std::vector<Author*> get_authors_from_repo()
{
    std::vector <Author *> authors {};
    std::string choice {"_"};
    std::string property {};
    bool want_exit {true};
    do
    {
        cout << "A - Выбрать по имени\n"
                "B - Выбрать по дате рождения\n"
                "D - Выбрать по дисциплине\n"
                "Q - Прервать\n";
        std::getline(std::cin, choice);
        if (choice.front() == 'Q')
            break;
        cout << "Введите значение этого поля: ";
        std::getline(std::cin, property);
        switch (choice.front())
        {
        case 'A':
            authors = List::get_authors_by_name(property);
            break;
        case 'B':
            authors = List::get_authors_by_date(property);
            break;
        case 'D':
            authors = List::get_authors_by_descipline(property);
            break;
        case 'Q':
            want_exit = true;
            break;
        default:
            want_exit = false;
            cout << "Повторите ввод!\n";
        }
    }
    while (!want_exit);
    return authors;
}

void sort_lists_in_repo()
{
    std::string choice {"_"};
    do
    {
        cout << "A - Отсортировать авторов\n"
                "B - Отсортировать книги\n"
                "Q - Прервать\n";
        std::getline(std::cin, choice);
        switch (choice.front())
        {
        case 'A':
            sort_authors_in_repo();
            break;
        case 'B':
            sort_books_in_repo();
            break;
        case 'Q':
            break;
        default:
            cout << "Повторите ввод!\n";
        }
    }
    while (choice.front() != 'Q' && choice.front() != 'A' && choice.front() != 'B');
}

void output_with_condition()
{
    std::string choice {"_"};
    do
    {
        cout << "A - Вывести авторов\n"
                "B - Вывести книги\n"
                "Q - Прервать\n";
        std::getline(std::cin, choice);
        switch (choice.front())
        {
        case 'A':
            output_authors_with_condition();
            break;
        case 'B':
            output_books_with_condition();
            break;
        case 'Q':
            break;
        default:
            cout << "Повторите ввод!\n";
        }
    }
    while (choice.front() != 'Q' && choice.front() != 'A' && choice.front() != 'B');
}

void output_within_the_range()
{
    std::string choice {"_"};
    do
    {
        cout << "A - Вывести авторов\n"
                "B - Вывести книги\n"
                "Q - Прервать\n";
        std::getline(std::cin, choice);
        switch (choice.front())
        {
        case 'A':
            output_authors_within_the_range();
            break;
        case 'B':
            output_books_within_the_range();
            break;
        case 'Q':
            break;
        default:
            cout << "Повторите ввод!\n";
        }
    }
    while (choice.front() != 'Q' && choice.front() != 'A' && choice.front() != 'B');
}

void output_authors_with_condition()
{
    auto authors = get_authors_from_repo();
    for (Author *author : authors)
    {
        cout << "{ " << //
                author->full_name << "; " << //
                author->birth_date << "; " << //
                author->desciplines << "; " <<//
                author->filename << "; " <<//
                " }\n";
    }
}
void output_books_with_condition()
{
    auto books = get_books_from_repo();
    for (Book *book : books)
    {
        cout << "{ " << //
                book->authors<< "; " << //
                book->title<< "; " << //
                book->publishing << "; " <<//
                book->year << "; " <<//
                book->isbn << "; " <<//
                " }\n";
    }
}

void output_authors_within_the_range()
{
    int min{-1}, max{-1};

    cout << "Введите нижнюю границу года рождения: ";
    cin >> min;
    cout << "Введите верхнюю границу года рождения: ";
    cin >> max;

    cin.ignore(2, '\n');

    auto authors = List::get_authors_by_period(min, max);
    for (Author *author : authors)
    {
        cout << "{ " << //
                author->full_name << "; " << //
                author->birth_date << "; " << //
                author->desciplines << "; " <<//
                author->filename << "; " <<//
                " }\n";
    }


}
void output_books_within_the_range()
{
    int min{-1}, max{-1};

    cout << "Введите нижнюю границу года издательства: ";
    cin >> min;
    cout << "Введите верхнюю границу года издательства: ";
    cin >> max;

    cin.ignore(2, '\n');

    auto books = List::get_books_by_period(min, max);
    for (Book *book : books)
    {
        cout << "{ " << //
                book->authors<< "; " << //
                book->title<< "; " << //
                book->publishing << "; " << //
                book->year << "; " << //
                book->isbn << "; " << //
                " }\n";
    }
}

void sort_authors_in_repo()
{
    auto cur_author_i = author_list;
    while (cur_author_i)
    {
        auto cur_author_j = cur_author_i->next;
        while (cur_author_j)
        {
            if(cur_author_i->data->full_name > cur_author_j->data->full_name)
            {
                author_list = swap_authors(author_list, cur_author_i, cur_author_j);
                auto tmp = cur_author_i;
                cur_author_i = cur_author_j;
                cur_author_j = tmp;
            }
            cur_author_j = cur_author_j->next;
        }
        cur_author_i = cur_author_i->next;
    }
}
void sort_books_in_repo()
{
    auto cur_author = author_list;
    while (cur_author)
    {
        auto cur_book_i = cur_author->data->bibliography;
        while (cur_book_i)
        {
            auto cur_book_j = cur_book_i->next;
            while (cur_book_j)
            {
                if(cur_book_i->data->title > cur_book_j->data->title)
                {
                    cur_author->data->bibliography = swap_books(cur_author->data->bibliography, cur_book_i, cur_book_j);
                    auto tmp = cur_book_i;
                    cur_book_i = cur_book_j;
                    cur_book_j = tmp;
                }

                cur_book_j = cur_book_j->next;
            }
            cur_book_i = cur_book_i->next;
        }

        cur_author = cur_author->next;
    }
}

}

namespace Str
{
    std::vector<std::string> split (const std::string &str, const char delim)
    {
        std::vector<std::string> strings{};
        int i = 0;
        size_t pos = str.find(delim);
        while (pos != string::npos)
        {
            strings.push_back(str.substr(i, pos-i));
            i = ++pos;
            pos = str.find(delim, pos);
        }
        strings.push_back(str.substr(i, str.length()));
        return strings;
    }
}
