#include <stdlib.h>
#include <stdio.h>


typedef struct Natural {
    int *number;//само число представленое массивом чисел
    int quan;//Количестово цифр в числе
} Natural;

Natural num_cpy(Natural Num) {
    Natural newNum;
    newNum.quan = Num.quan;
    newNum.number = (int*)malloc(sizeof(int));
    for (int i = 0; i < Num.quan; i++) {
        newNum.number = (int*)realloc(newNum.number, (i + 1) * sizeof(int));
        newNum.number[i] = Num.number[i];
    }
    return newNum;
}

/**
 * @author Золотухин М.А.
 * created on 2016/04/24
 * 
 * @brief Проверяет является ли число нулем или нет
 * 
 * @param num Число для сравнения с нулем
 * @return 1, если число не равно нулю, 0, если нет
 * 
 */
int NZER_N_B (Natural* num) 
{
    // Если число существует, и старший разряд 0
    if (num && num->number[num->quan - 1] == 0) 
        return 0;
    else
        return 1;
}

/**
 * @author Золотухин М.А.
 * created on 2016/04/24
 * 
 * @brief Добавляет 1 к натуральному числу
 * @details К младшему разряду прибавляется 1, 
 *          если разряд переполняется, то он
 *          обнуляется, а 1 прибавляется к следующему,
 *          который в свою очередь тоже может
 *          переполниться и тд. Если переполняется
 *          последний разряд, создается новый 
 *          со значением 1
 * 
 * @param num Число к которому прибавляется единица
 * @return Число на единицу большее исходного
 */
Natural* ADD_1N_N (Natural* num) 
{
    if (num) {
        // Возвращать указатели на статические переменные 
        // нельзя, поэтому выделяем память динамически
        Natural *new_num = (Natural*)malloc(sizeof(Natural));
        if (!new_num) {
            printf("Не хватает памяти\n");
            return NULL;
        }
        *new_num = num_cpy(*num);
        
        // Добавляем 1 и, при необходимости, сдвигаем разряд
        int i = 0;
        while (i < num->quan && ++(new_num->number)[i] > 9) {
            (new_num->number)[i] = 0;
            i++;
        }

        // Добавление 1 разряда сверху, если цифр не хватило
        if (i == num->quan) {
            (new_num->quan)++;
            new_num->number = (int*)realloc(new_num->number, (new_num->quan)*sizeof(int));
            if (!new_num) {
                printf("Не хватает памяти\n");
                return NULL;
            }

            (new_num->number)[i] = 1;
        }

        return new_num;
    } else
        return NULL;
}

/**
 * @author Золотухин М.А.
 * created on 2016/04/24
 * 
 * @brief Умножает натуральное число на 10^k
 * @details Умножение на 10 - добавление нулей в начало
 * 
 * @param num Умножаемое число
 * @param k Степень десятки, на которую умножается число
 * @return Умноженное число
 */
Natural* MUL_Nk_N (Natural* num, int k) 
{
    if (num) {
        // Если число 0, то результат всегда ноль
        // Возращаем 0 в виде Natural
        if(!NZER_N_B(num)) {
            Natural * zero = (Natural*)malloc(sizeof(Natural));
            zero->quan = 1;
            zero->number = (int*)malloc(sizeof(int));
            zero->number[0] = 0;
            return zero;
        }

        // Если k ноль, то никаких сдвигов не нужно, 
        // просто возвращаем исходное число
        if (k == 0) {
            // Создаем копию
            Natural *new_num = (Natural*)malloc(sizeof(Natural));
            if (!new_num) {
                printf("Не хватает памяти\n");
                return NULL;
            }
            *new_num = num_cpy(*num);
            //Ничего не меняем
            return new_num;
        }

        else if (k > 0) {
            // Создаем копию
            Natural *new_num = (Natural*)malloc(sizeof(Natural));
            if (!new_num) {
                printf("Не хватает памяти\n");
                return NULL;
            }
            *new_num = num_cpy(*num);

            // Увеличиваем количество разрядов
            (new_num->quan)+= k;
            // Перевыделяем память
            new_num->number = (int*)realloc(new_num->number, (new_num->quan)*sizeof(int));
            if (!new_num) {
                printf("Не хватает памяти\n");
                return NULL;
            }
            // Сдвигаем массив на k позиций вперед
            int i; 
            // i - разряд в который копируют
            // i-k - разряд в который копируют
            for (i = new_num->quan - 1; i >= k; i--) 
                (new_num->number)[i] = (new_num->number)[i-k];
            
            // Дальше заполняем оставшиеся разряды нулями
            for (; i >= 0; i--)
                (new_num->number)[i] = 0;
            return new_num;

        } else if (k < 0) {
            // Делаем k положительным, чтобы не путаться в дальнейшем
            k = -k;
            // Проверка на то, что число делится нацело на 10^k
            // То есть есть ли в конце достаточное количество нулей
            int is_10k = 1, i = 0;
            while (i < k && is_10k) {
                if ((num->number)[i] != 0) 
                    is_10k = 0;
                i++;
            }
            if (is_10k) {
                // Создаем копию
                Natural *new_num = (Natural*)malloc(sizeof(Natural));
                if (!new_num) {
                    printf("Не хватает памяти\n");
                    return NULL;
                }
                *new_num = num_cpy(*num);

                // Сдвигаем массив на k позиций назад (убираем лишние нули)
                int i; 
                // i - разряд в который копируют
                // i-k - разряд из которого копируют
                for (i = 0; i < (new_num->quan) - k; i++) 
                    (new_num->number)[i] = (new_num->number)[i+k];

                // Убираем лишние разряды
                (new_num->quan)-= k;
                // Перевыделяем память
                new_num->number = (int*)realloc(new_num->number, (new_num->quan)*sizeof(int));
                if (!new_num) {
                    printf("Не хватает памяти\n");
                    return NULL;
                }

                return new_num;
            } else {
                printf("Число не делится нацело\n");
                return NULL;
            }
        }
    } else 
        return NULL;
}

typedef struct Integer {
    int *number;
    int quan;
    int sign;//знак
} Integer;

typedef struct Ratio {
    Integer Numer;
    Natural Denom;
} Ratio;

typedef struct Polinomial {
    Ratio *koefP;
    int n; // кол-во степеней
} Polinomial;
/*
Ratio MUL_QQ_Q(Ratio first_coef, Ratio second_coef);
*/
/**
 * @author Золотухин М.А.
 * created on 2016/04/24
 * 
 * @brief Ищет производную многочлена
 * @details Ищет производную каждого члена по отдельности и 
 *          записывает их в новый массив
 * 
 * @param polym Многочлен, чья производная ищется
 * @return Искомая производная многочлена
 */
/*
Polinomial* DER_P_P (Polinomial* polim) 
{
    if (polim) {
        // Создаем новый многочлен со степенью меньше исходного на 1
        Polinomial* der = (Polinomial*)malloc(sizeof(Polinomial));
        if (!der) {
            printf("Не хватает памяти\n");
            return NULL;
        }
        der->n = polim->n - 1;
        der->koefP = (Ratio*)malloc((der->n)*sizeof(Ratio));
        if (!(der->koefP)) {
            printf("Не хватает памяти\n");
            return NULL;
        }

        // Проходим производной по каждой степени
        // Начинаем с 1, т.к. (const)' = 0 
        int i;
        for (i = 1; i <= der->n; i++) {
            // Первый коэффициент перед x^i - 
            // выносится за производную
            Ratio first_coef = polim->koefP[i];
            // Степень x в виде рационального числа для сложения
             
            int* den = (int*)malloc(sizeof(int)); // Значенатель дроби 
            *den = 1;         // Выглядит все некрасиво, но так как
                              // нормального преобразования типов
                              // из int в Natural нет в списках функций
                              // то сделано так.
                              // По-хорошему нужжно так:
                              // TRANS_Z_Q( TRANS_N_Z( int_to_N(i) ) );
                              // Так же можно написать фунцкцию умножения на 
                              // int для рациональных чисел
            
            Ratio second_coef = {{&i, 1, 0}, {den, 1}};
            // Умножаем коэффициенты 
            Ratio result = MUL_QQ_Q(first_coef, second_coef);
            // Присваеваем получнный коэффициент новому многочлену
            der->koefP[i-1] = result;
        }
        return der;
    } else
        return NULL;
}
*/

/**
 * @author Золотухин М.А.
 * created on 2016/04/26
 * 
 * @brief Умножает многочлен на x^k
 * 
 * @param polim Умножаемый многочлен
 * @param k Степень х
 * 
 * @return Умноженный многочлен
 */
Polinomial* MUL_Pxk_P (Polinomial* polim, int k)
{
    if (polim) {
        // Если x^0 = 1, то не умножаем - делаем копию и возвращаем
        if (k == 0) {
            Polinomial* result = (Polinomial*)malloc(sizeof(Polinomial));
            if (!result) {
                printf("Не хватает памяти\n");
                return NULL;
            }
            result->n = polim->n;
            result->koefP = (Ratio*)malloc((result->n)*sizeof(Ratio));
            if (!(result->koefP)) {
                printf("Не хватает памяти\n");
                return NULL;
            }
            int i;
            for (i = 0; i < result->n; i++) 
                result->koefP[i] = polim->koefP[i];
            
            return result;

        // Если степень положительна, то степень мн-на повышается
        // значит создаем массив на k элементов больше, первые k 
        // заполняем нулями, а остальные коэффициенты копируем
        } else if (k > 0) {
            Polinomial* result = (Polinomial*)malloc(sizeof(Polinomial));
            if (!result) {
                printf("Не хватает памяти\n");
                return NULL;
            }
            // Увеличиваем длину
            result->n = polim->n + k;
            result->koefP = (Ratio*)malloc((result->n)*sizeof(Ratio));
            if (!(result->koefP)) {
                printf("Не хватает памяти\n");
                return NULL;
            }
            int i;
            for (i = 0; i < k; i++) {
                // Ноль в виде Рационального числа
                int* den = (int*)malloc(sizeof(int)); // Значенатель дроби 
                *den = 1;
                int* nom = (int*)malloc(sizeof(int)); // Значенатель дроби 
                *nom = 0;
                Ratio zero = {{nom, 1, 0}, {den, 1}};
                result->koefP[i] = zero;
            }
            for (i = k; i < result->n; i++) 
                result->koefP[i] = polim->koefP[i];

            return result;

        // Если степень отрицательна, то неоходима проверка, 
        // что не образуется отрицательных степеней х
        // для этого необходимо, чтобы степень многочлена была >= k
        // (степень многочлена = n-1), а коэффициенты при младших х
        // не делящихся нацело на х^k были нулевыми
        } else if (k < 0) {
            // Для определенности делаем k положительным (будем делить на x^k)
            k = -k;
            if (polim->n - 1 >= k) {
                // Проверяемвсе ли коэффициенты до x^k в многочлене нули
                int i, is_zero = true;

                for (i = 0; i < k && is_zero; i++) {
                    Integer nom = (polim->koefP[i]).Numer; // Числитель дроби
                    if (nom.number[nom.quan - 1] != 0) // Проверка, что число ноль
                        is_zero = false;
                }

                // Если прошли по условию, то создаем 
                if (is_zero) {
                    Polinomial* result = (Polinomial*)malloc(sizeof(Polinomial));
                    if (!result) {
                        printf("Не хватает памяти\n");
                        return NULL;
                    }
                    // Уменьшаем длину
                    result->n = polim->n - k;
                    result->koefP = (Ratio*)malloc((result->n)*sizeof(Ratio));
                    if (!(result->koefP)) {
                        printf("Не хватает памяти\n");
                        return NULL;
                    }

                    // Записываем коэффициенты, не забываем сдвиг в исходном 
                    // многочлене, т.к. первые должны быть нулями
                    for (i = 0; i < result->n; i++) 
                        result->koefP[i] = polim->koefP[i+k];

                    return result;

                } else {
                    printf("Невозможно поделить нацело\n");
                    return NULL;
                }
            } else {
                printf("Невозможно поделить нацело\n");
                return NULL;
            }
        }
    } else
        return NULL;
}

int main()
{
    /*
    // Tests for ADD_1N_N
    int numbers_arr[] = {9,9,9,9, 9};
    Natural one = {numbers_arr, 5};

    int right_num_arr[] = {0,0,0,0,0, 1};
    Natural right_answer = {right_num_arr, 6};
    
    Natural* cur_answer = ADD_1N_N(&one);
    
    printf("Правильный ответ:\n");
    printf("%d\n", right_answer.quan);
    int i;
    for (i = 0; i < right_answer.quan; i++)
        printf("%d ", right_answer.number[i]);
    printf("\n");
    printf("\n");


    printf("Полученный ответ:\n");
    printf("%d\n", cur_answer->quan);
    // int i;
    for (i = 0; i < cur_answer->quan; i++)
        printf("%d ", cur_answer->number[i]);
    printf("\n");

    */
    /*
    // Tests for MUL_Nk_N
    int numbers_arr[] = {0,0,0,0,0,4,5,2,4,2,2,3};
    Natural input_natural = {numbers_arr, 12};
    int k = -5;

    int right_num_arr[] = {4,5,2,4,2,2,3};
    Natural right_answer = {right_num_arr, 7};
    
    Natural* cur_answer = MUL_Nk_N(&input_natural, k);
    
    printf("Правильный ответ:\n");
    printf("%d\n", right_answer.quan);
    int i;
    for (i = 0; i < right_answer.quan; i++)
        printf("%d ", right_answer.number[i]);
    printf("\n");
    printf("\n");

    if (cur_answer) {
        printf("Полученный ответ:\n");
        printf("%d\n", cur_answer->quan);
        // int i;
        for (i = 0; i < cur_answer->quan; i++)
            printf("%d ", cur_answer->number[i]);
        printf("\n");
    }
    */

    // Tests for MUL_Pxk_P
    int numbers_arr[] = {0,0,0,0,0,4,5,2,4,2,2,3};
    Natural input_natural = {numbers_arr, 12};
    int k = -5;

    int right_num_arr[] = {4,5,2,4,2,2,3};
    Natural right_answer = {right_num_arr, 7};
    
    Natural* cur_answer = MUL_Nk_N(&input_natural, k);
    
    printf("Правильный ответ:\n");
    printf("%d\n", right_answer.quan);
    int i;
    for (i = 0; i < right_answer.quan; i++)
        printf("%d ", right_answer.number[i]);
    printf("\n");
    printf("\n");

    if (cur_answer) {
        printf("Полученный ответ:\n");
        printf("%d\n", cur_answer->quan);
        // int i;
        for (i = 0; i < cur_answer->quan; i++)
            printf("%d ", cur_answer->number[i]);
        printf("\n");
    }

    // Tests for DER_P_P (NOT NOW)
    return 0;
}

